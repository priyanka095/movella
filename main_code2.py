import csv
tmIDs=set()
year_=[]
total_wins=0
playerID_=[]
total_losses=0
total_games_played=0
total_minutes_played=0
total_goals_against=0
total_shots_against=0
most_goals_stopped={}
goals_stopped={}
efficient={}

with open(r'Goalies.csv', "r", newline='')as f1:
    # csv_data = list(csv.reader(f1))
    r = csv.DictReader(f1)
    csv_data_dict = list(r)

for data in csv_data_dict:
    tmIDs.add(data['tmID'])
print('Team Ids :',list(tmIDs))

team_id=input('Select the Team id from above List:').upper()
year =int(input('enter the year from 1909 to 2011:'))
print(team_id,'-',year)



def played_years(team_id):
    year_list = set()
    for data in csv_data_dict:
        if data['tmID'] == team_id:
            year_list.add(int(data['year']))
    return year_list

played_years = played_years(team_id)
if year in played_years:
    year=year
else:
    print(team_id + ' team not played in the year :' + str(year))
    print(team_id + ' team has played games in the following years : ', list(played_years))
    new_year = int(input('Select the correct year from above years:'))
    year=new_year
def result(team_id,year):
    global total_wins, total_losses, total_games_played, total_minutes_played, total_goals_against, total_shots_against
    print(team_id,year)
    for row in csv_data_dict:
        # print(row)
        if row['tmID']==team_id and int(row['year'])==year:
            #3 Wins_agg: total wins / total players
            if len(row['W']):
                total_wins = total_wins + int(row['W'])
            if len(row['playerID']) > 1:
                playerID_.append(row['playerID'])
            #4 Losses_agg: total losses / total players
            if len(row['L']):
                total_losses = total_losses + int(row['L'])
            #5 GP_agg: total games played / total players
            if len(row['GP']) > 1:
                total_games_played=total_games_played+int(row['GP'])
            # 6Mins_over_GA_agg: total minutes played / total goals against
            if len(row['Min']) > 0:
                total_minutes_played = total_minutes_played + int(row['Min'])
            if len(row['GA']) > 0:
                total_goals_against = total_goals_against + int(row['GA'])
            # 7. GA_over_SA_agg: total goals against / total shots against
            print()
            if len(row['SA'])>=1:
                total_shots_against = total_shots_against + int(row['SA'])

            # most_goals_stopped: {‘playerID’: playerID, ‘goals_stopped’: goals_stopped}
            if int(row['GA'])>=1:
                goals_stopped[row['playerID']]=int(row['GA'])
            # most_efficient_player: {‘playerID’: playerID, ‘efficiency’: goals_stopped / minutes played}
            if int(row['GA'])>=1 and int(row['Min'])>=1:
                efficient1=(int(row['GA']))/(int(row['Min']))
                efficient[row['playerID']]=(efficient1)


    try:
        try:
            print('Wins_agg:', total_wins / len(set(playerID_)))
        except Exception as e:
            # print(e)
            if len(set(playerID_))==0:
                divider=1
                print('Wins_agg:', total_wins / divider)
        try:
            print('Losses_agg:', total_losses / len(set(playerID_)))
        except Exception as e:
            if len(set(playerID_))==0:
                divider=1
                print('Wins_agg:', total_wins / divider)
        try:
            print('GP_agg:', (total_games_played) / len(set(playerID_)))
        except:
            if len(set(playerID_))==0:
                divider=1
                print('Wins_agg:', total_wins / divider)
        try:
            print('Mins_over_GA_agg:', total_minutes_played / total_goals_against)
        except:
            if total_goals_against==0:
                divider=1
                print('Wins_agg:', total_wins / divider)
        try:
            print('GA_over_SA_agg:', total_goals_against / total_shots_against)
        except:
            if total_shots_against==0:
                divider=1
                print('Wins_agg:', total_wins / divider)

        print(goals_stopped)

        # most_goals_stopped: {‘playerID’: playerID, ‘goals_stopped’: goals_stopped}
        key_list = list(goals_stopped.keys())
        val_list = list(goals_stopped.values())
        max_value=max(val_list)
        # print(key_list,max_value)
        position = val_list.index(max_value)
        # print(key_list[position])
        print('most_goals_stopped:',{'playerID':key_list[position],'goals_stopped':max_value})
        # most_efficient_player: {‘playerID’: playerID, ‘efficiency’: goals_stopped / minutes played}
        #print(efficient)
        key_list = list(efficient.keys())
        val_list = list(efficient.values())
        max_value=max(val_list)
        # print(key_list,max_value)
        position = val_list.index(max_value)
        #print(key_list[position])
        print('most_goals_stopped:', {'playerID': key_list[position], 'goals_stopped': max_value})
        print('avg_percentage_wins:', (total_wins / len(set(playerID_))))

    except Exception as e:
        print('error',e)
result(team_id,year)